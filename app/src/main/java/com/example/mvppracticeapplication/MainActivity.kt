package com.example.mvppracticeapplication


import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

  lateinit var  SharedPreferences:SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        SharedPreferences = getSharedPreferences(mypreference, MODE_PRIVATE)
        intiView()
    }

    private fun intiView() {
        if (SharedPreferences.contains(Name)) {
            edtName.setText(SharedPreferences.getString(Name, ""))

        }
        if (SharedPreferences.contains(Email)) {
            edtEmail.setText(SharedPreferences.getString(Email, ""))

        }

    }

    fun Save(view: View){
        val name = edtName.getText().toString()
        val email = edtEmail.getText().toString()
        val editor: Editor = SharedPreferences.edit()
        editor.putString(Name, name)
        editor.putString(Email, email)
        editor.apply()
    }
    fun clear(view: View){
        edtName.setText("")
        edtEmail.setText("")

    }
    fun Get(view: View){
        if (SharedPreferences.contains(Name)) {
            edtName.setText(SharedPreferences.getString(Name, ""))
        }
        if (SharedPreferences.contains(Email)) {
            edtEmail.setText(SharedPreferences.getString(Email, ""))

        }

    }

   
}